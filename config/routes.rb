Rails.application.routes.draw do
  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  get "wotm/config/:wad_name/:user_submitted" => "wotm#get_config"
  get "wotm/name/:wad_name/:user_submitted" => "wotm#get_by_name"
  post "wotm/name/:wad_name" => "wotm#new_wad"
  get "wotm/available" => "wotm#get_available"
  get "wotm/current" => "wotm#get_current_wotm"

  mount ActionCable.server => '/cable'
end
