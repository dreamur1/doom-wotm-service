require_relative "boot"

require "action_cable/engine"
require "action_controller/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DoomWotmService
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w(assets tasks))

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    def create_rabbit_listener_thread
      Thread.new do
        Rails.application.executor.wrap do
          puts "[*] Creating rabbit listener thread"

          connection = Bunny.new(automatically_recover: false, host: ENV.fetch("RABBIT_HOST", "doom-wotm-rabbitmq-1"), port: 5672, user: ENV.fetch("RABBITMQ_DEFAULT_USER", "user"), pass: ENV.fetch("RABBITMQ_DEFAULT_PASS", "password"))
          connection.start

          created_wads_channel = connection.create_channel
          created_wads_queue = created_wads_channel.queue('wad-completed-jobs', durable: true)

          created_wads_channel.prefetch(1)
          puts ' [*] Listening for Completed WADs'

          begin
              created_wads_queue.subscribe(manual_ack: true, block: true) do |delivery_info, _properties, body|
                  obsidianConfig = ObsidianConfig.new.extractConfigValsFromString body
                  puts "[*] Recieved news that wad named: #{obsidianConfig.wadName} is completed!"
                  ActionCable.server.broadcast("wad-completed-jobs", obsidianConfig)
                  created_wads_channel.ack(delivery_info.delivery_tag)
              end
          rescue Interrupt => _
              connection.close
          end
        end
      end      
    end
  end
end
