class WotmController < ApplicationController
    attr_reader :wadsPath
    
    def initialize
        @wadsPath = "/obsidian/wads/"
        @wotmPath = "/obsidian/wotm/"
    end

    def get_by_name
        if !isValidInputString(params[:wad_name])
            return render :js => {:response_type => "ERROR", :response_code => 400, :message => "The requested Wad name can only contain alphanumeric chars and '-'"}.to_json, :status => 400
        end

        isUserSubmitted = params[:user_submitted]
        requestedWad = "#{@wotmPath}/#{params[:wad_name]}.pk3" 

        if isUserSubmitted == "true"
            requestedWad = "#{@wadsPath}/#{params[:wad_name]}.pk3"
            if !File.exist?(requestedWad) or File.stat(requestedWad).ctime + (60 * 60 * 24) <= Time.now
                raise ActionController::RoutingError.new('Not Found')
            end
        end

        send_file requestedWad
    end

    def get_available
        returnData = []

        allWads = Dir["#{@wadsPath}*.pk3"]
        
        for i in 0 .. (allWads.length-1) do
            deletionTimestamp = File.stat(allWads[i]).ctime + (60 * 60 * 24)
            if deletionTimestamp <= Time.now
                next
            end

            obsidianConfig = ObsidianConfig.new.extractConfigVals "#{allWads[i].gsub('.pk3', '')}.cfg.txt"

            tmpVal = {
                wadName: obsidianConfig.wadName,
                deletionTimestamp: deletionTimestamp,
                obsidianConfig: obsidianConfig
            }

            returnData.push(tmpVal)
        end

        returnData = returnData.sort_by { |val| val[:deletionTimestamp] }.reverse
        render json: returnData.to_json
    end

    def get_config
        if !isValidInputString(params[:wad_name])
            return render :js => {:response_type => "ERROR", :response_code => 400, :message => "The requested config name can only contain alphanumeric chars and '-'"}.to_json, :status => 400
        end

        isUserSubmitted = params[:user_submitted]
        requestedConfig = "#{@wotmPath}/#{params[:wad_name]}.cfg.txt"

        if isUserSubmitted == "true"
            requestedConfig = "#{@wadsPath}/#{params[:wad_name]}.cfg.txt"
            if !File.exist?(requestedConfig) or File.stat(requestedConfig).ctime + (60 * 60 * 24) <= Time.now
                raise ActionController::RoutingError.new('Not Found')
            end
        end

        obsidianConfig = ObsidianConfig.new.extractConfigVals requestedConfig
        render json: obsidianConfig.to_json
    end

    def new_wad
        requestedWad = params[:wad_name]

        if !isValidInputString(requestedWad)
            return render :js => {:response_type => "ERROR", :response_code => 400, :message => "The requested Wad name can only contain alphanumeric chars and '-'"}.to_json, :status => 400
        end

        if requestedWad.length > 25
            return render :js => {:response_type => "ERROR", :response_code => 400, :message => "WAD names can be no longer than 25 characters long"}.to_json, :status => 400
        end

        connection = Bunny.new(automatically_recover: false, host: ENV.fetch("RABBIT_HOST", "doom-wotm-rabbitmq-1"), port: 5672, user: ENV.fetch("RABBITMQ_DEFAULT_USER", "user"), pass: ENV.fetch("RABBITMQ_DEFAULT_PASS", "password"))
        connection.start

        channel = connection.create_channel
        queue = channel.queue('wad-requests', durable: true)

        channel.default_exchange.publish(request.raw_post, routing_key: queue.name)
        connection.close

        render :js => {:response_type => "basic", :response_code => 200, :message => "Wad name added to queue"}.to_json, :status => 200
    end

    def get_current_wotm
        returnData = []

        wotmConfigs = Dir["#{@wotmPath}*.cfg.txt"]

        currentTime = Time.now
        deletionTimestamp = Time.new(currentTime.year, (currentTime.month % 12) + 1, 1, 0, 0, 0)
        
        for i in 0 ... wotmConfigs.length do
            obsidianConfig = ObsidianConfig.new.extractConfigVals "#{wotmConfigs[i]}"

            tmpVal = {
                wadName: obsidianConfig.wadName,
                deletionTimestamp: deletionTimestamp,
                obsidianConfig: obsidianConfig
            }

            returnData.push(tmpVal)
        end

        render json: returnData.to_json
    end

    def isValidInputString(str)
        if str.match? /[^a-zA-z0-9-]/
            return false
        end
        true
    end
end