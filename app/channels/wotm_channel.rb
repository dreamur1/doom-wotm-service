class WotmChannel < ApplicationCable::Channel
    def subscribed
        stream_from "wad-completed-jobs"
    end
end