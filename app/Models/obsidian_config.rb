class ObsidianConfig
    attr_accessor :wadName, :formType, :deletionTimestamp
    attr_accessor :sourceWad, :gameLength
    attr_accessor :levelSize, :allowPrebuiltLevels, :lightingMultiplier, :outdoorsFrequency, :cavesFrequency, :liquidsFrequency, :hallwaysFrequency, :teleportersFrequency, :enableBottomlessVistasAndSkyboxes
    attr_accessor :monsterQuantity, :monsterStrength, :pistolStartCompatable, :safeStartingRooms, :monsterVariety, :trapQuantity, :monstersInSecrets
    attr_accessor :healthPickupQuantity, :ammoPickupQuantity, :miscItemQuantity, :secretsQuantity, :secretsBonus
    attr_accessor :zombieCount, :shotgunnerCount, :sargeCount, :naziCount, :impCount, :lostSoulCount, :demonCount, :spectreCount, :painElementalCount, :cacodemonCount, :hellKnightCount, :revenantCount, :mancubusCount, :arachnatronCount, :archVileCount, :baronOfHellCount, :cyberDemonCount, :spiderMastermindCount

    def initialize
        @wadName = ""
        @formType = "basic"

        # Game Settings
        @sourceWad = "doom2"
        @gameLength = "game"

        # Architecture
        @levelSize = 100
        @allowPrebuiltLevels = true
        @lightingMultiplier = 100
        @outdoorsFrequency = "mixed"
        @cavesFrequency = "mixed"
        @liquidsFrequency = "mixed"
        @hallwaysFrequency = "mixed"
        @teleportersFrequency = "mixed"
        @enableBottomlessVistasAndSkyboxes = false

        # Combat
        @monsterQuantity = 100
        @monsterStrength = 100
        @pistolStartCompatable = true
        @safeStartingRooms = false
        @monsterVariety = "mixed"
        @trapQuantity = "mixed"
        @monstersInSecrets = false

        # Pickups
        @healthPickupQuantity = "normal"
        @ammoPickupQuantity = "normal"
        @miscItemQuantity = "normal"
        @secretsQuantity = "mixed"
        @secretsBonus = "none"

        # Other Modules
        @zombieCount = "Default"
        @shotgunnerCount = "Default"
        @sargeCount = "Default"
        @naziCount = "Default"
        @impCount = "Default"
        @lostSoulCount = "Default"
        @demonCount = "Default"
        @spectreCount = "Default"
        @painElementalCount = "Default"
        @cacodemonCount = "Default"
        @hellKnightCount = "Default"
        @revenantCount = "Default"
        @mancubusCount = "Default"
        @arachnatronCount = "Default"
        @archVileCount = "Default"
        @baronOfHellCount = "Default"
        @cyberDemonCount = "Default"
        @spiderMastermindCount = "Default"
    end

    def extractConfigVals(configName)
        configHash = Hash.new

        File.open(configName, "r") do |f|
            f.each_with_index do |line, index|
                if index == 0
                    @wadName = line.split(" ")[1]
                end
                if line.include? "="
                    splitLine = line.split("=")
                    configHash[splitLine[0].strip] = splitLine[1].strip
                end
            end
        end

        updateProperties(configHash)
    end

    def extractConfigValsFromString(inlineConfig) 
        configHash = Hash.new
        index = 0

        inlineConfig.each_line do |line|
            if index == 0
                @wadName = line.split(" ")[1]
                index += 1
            end
            if line.include? "="
                splitLine = line.split("=")
                configHash[splitLine[0].strip] = splitLine[1].strip
                index += 1
            end
        end

        @deletionTimestamp = Time.now + (60 * 60 * 24)
        updateProperties(configHash)
    end

    def updateProperties(configHash)
        @formType = configHash['-- formType']

        @sourceWad = configHash['game']
        @gameLength = configHash['length']
        @levelSize = (configHash['float_size'].to_f / 0.36).round

        @allowPrebuiltLevels = configHash['bool_prebuilt_levels'] ? true : false
        @lightingMultiplier = (configHash['float_overall_lighting_mult'].to_f * 100.0).round
        @outdoorsFrequency = configHash['outdoors'] 
        @cavesFrequency = configHash['caves'] 
        @liquidsFrequency = configHash['liquids'] 
        @hallwaysFrequency = configHash['hallways'] 
        @teleportersFrequency = configHash['teleporters'] 
        @enableBottomlessVistasAndSkyboxes = configHash['zdoom_vista'] == 'disable' ? false : true

        @monsterQuantity = (configHash['float_mons'].to_f * 100.0).round
        @monsterStrength = (configHash['float_strength'].to_f * 100.0).round
        @pistolStartCompatable = configHash['bool_pistol_starts'].to_i == 1
        @safeStartingRooms = configHash['bool_quiet_start'].to_i == 1
        @monsterVariety = configHash['mon_variety']
        @trapQuantity = configHash['traps']
        @monstersInSecrets = configHash['secret_monsters'] == 'yesyes' ? true : false

        @healthPickupQuantity = configHash['health']
        @ammoPickupQuantity = configHash['ammo']
        @miscItemQuantity = configHash['items']
        @secretsQuantity = configHash['secrets']
        @secretsBonus = configHash['secrets_bonus']

        @zombieCount = (configHash['float_zombie'].to_f * 5.0).round
        @shotgunnerCount = (configHash['float_shooter'].to_f * 5.0).round
        @sargeCount = (configHash['float_gunner'].to_f * 5.0).round
        @naziCount = (configHash['float_ss_nazi'].to_f * 5.0).round
        @impCount = (configHash['float_imp'].to_f * 5.0).round
        @lostSoulCount = (configHash['float_skull'].to_f * 5.0).round
        @demonCount = (configHash['float_demon'].to_f * 5.0).round
        @spectreCount = (configHash['float_spectre'].to_f * 5.0).round
        @painElementalCount = (configHash['float_pain'].to_f * 5.0).round
        @cacodemonCount = (configHash['float_caco'].to_f * 5.0).round
        @hellKnightCount = (configHash['float_knight'].to_f * 5.0).round
        @revenantCount = (configHash['float_revenant'].to_f * 5.0).round
        @mancubusCount = (configHash['float_mancubus'].to_f * 5.0).round
        @arachnatronCount = (configHash['float_arach'].to_f * 5.0).round
        @archVileCount = (configHash['float_vile'].to_f * 5.0).round
        @baronOfHellCount = (configHash['float_baron'].to_f * 5.0).round
        @cyberDemonCount = (configHash['float_Cyberdemon'].to_f * 5.0).round
        @spiderMastermindCount = (configHash['float_Spiderdemon'].to_f * 5.0).round

        self
    end
end