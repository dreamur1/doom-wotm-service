# Random Doom Lan Party Service

A fan-made api for the 'WAD of the Month' fan site. This is written to be used alongside the corresponding [frontend](https://gitlab.com/dreamur1/doom-wotm-site).

## Running it yourself

Start with a `bundle install` to install all dependencies.
Then, the best way to run this locally is from within its docker container. I usually do something like `docker build -t wotm-service .` && then run it with a `docker compose up -d` command (after configuring an appropriate compose file).
The Ruby on rails port is 3000 by default.

## Credits / Dependencies

- Ruby version 3.2.2
- [Ruby on Rails](https://rubyonrails.org/) version 7.1.2
